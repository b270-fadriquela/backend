console.log("Hello World");

let firstName = "Rafael", lastName = "Fadriquela", age = 30;
let hobbies = ["Video Games","Reading Manga","Puzzles"]

let workAddress = {
	houseNumber: 31,
	street: "Redbud",
	city: "Pasig",
	state: "Cainta",
}


console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies: " + hobbies);
console.log("Work Address:");
console.log(workAddress);



let fullName = "Steve Rogers";
console.log("My full name is" + fullName);

let currentAge = 40;

console.log("My current age is: " + currentAge);

let friends = ["Tony","Bruce", "Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false,

}
console.log("My Full Profile: ")
console.log(profile);

fullName = "Bucky Barnes";
console.log("My bestfriend is: " + fullName);

let lastLocation = "Arctic Ocean";
//const lastLocation = "Arctic Ocean";
//value declared as constant
//lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);






//CHECK LIST

//✅ 1. In the S15 folder, create an activity folder, an index.html file inside of it and link the index.js file.
//✅ 2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
//✅ 3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
//✅ 4. Create variables to store to the following user details:
//✅ first name - String
//✅ last name - String
//✅ age - Number
// hobbies - Array
// work address - Object
// 5. The hobbies array should contain at least 3 hobbies as Strings.
// 6. The work address object should contain the following key-value pairs:
// houseNumber: <value>
// street: <value>
// city: <value>
// state: <value>
// 7. Log the values of each variable to follow/mimic the output.
// 8. Identify and implement the best practices of creating and using variables by avoiding errors and debugging the following codes. 
// 9. Log the values of each variable to follow/mimic the output.
// 10. Create a git repository named S15.
// 11. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 12. Add the link in Boodle.