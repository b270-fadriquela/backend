



let x = 10, y = 12;

let sum = x + y;
let difference = x - y;
let product = x * y;
let ratio = y / x;

let remainder =  y % x;


let assignmentNum = 8;
//assignmentNum = assignmentNum + 2;

assignmentNum += 2;




console.log("Result of addition: " + sum);
console.log("Result of subraction: " + difference);
console.log("Result of product: " + product);
console.log("Result of ratio: " + ratio);
console.log("Result of modular: " + remainder);


console.log("result of assignment: "  + assignmentNum);


//Shorthand
assignmentNum += 2;
console.log(assignmentNum );

assignmentNum -= 2;
console.log(assignmentNum );

assignmentNum *= 2;
console.log(assignmentNum );

assignmentNum /= 2;
console.log(assignmentNum );

assignmentNum %= 2;
console.log(assignmentNum );


let num = 1 + 2 - 3 * 4 / 5
console.log(num);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);


let numA = "10";
let numB = 12;

let coercion = numA + numB;

console.log(coercion);
console.log(typeof(coercion));

let numC = 16, numD = 14;
let noCoercion = numD+ numC;

console.log(noCoercion)
console.log(typeof noCoercion)

let numE = true + 1;

console.log(numE);


let juan = "juan";

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log(0 == false);
console.log("juan" =="juan");
console.log("juan" == juan);

console.log("InEQUALITY!!!!");

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log(0 != false);
console.log("juan" !="juan");
console.log("juan" != juan);


console.log("Strickly Equality");

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log(0 === false);
console.log("juan" ==="juan");
console.log("juan" === juan);

console.log("Strickly InEQUALITY");

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== "1");
console.log(0 !== false);
console.log("juan" !=="juan");
console.log("juan" !== juan);




let a = 50, b = 65;

console.log("relational operators")

let isGreaterThan = a > b;
let isLessThan = a < b;
let isGtorEqual = a >= b;
let isLtorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGtorEqual);
console.log(isLtorEqual);

console.log("===")

let isLegalAge = true;
let isRegistered = false;

let allReguirement = isLegalAge && isRegistered;
let partialRequrement = isLegalAge || isRegistered;
let partialRequrementNotMet = !isRegistered;

console.log(allReguirement);
console.log(partialRequrement);

console.log(partialRequrementNotMet);


let z = 1;

let increment = ++z;
console.log("increment value: " + increment);
console.log("z: " + z);


increment = z++;
console.log("increment value: " + increment);
console.log("z: " + z);