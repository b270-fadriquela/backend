/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	fetchUserData();
	//first function here:
		function fetchUserData(){
			let fullName = prompt("What is your Name?");
			let age = prompt("How old are you?");
			let location = prompt("Where do you live?");


			console.log("Hello, " + fullName);
			console.log("You are " + age + " years old");
			console.log("Location: " + location);	

			alert("Thanks for your input!");
		}


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/


	fetchTopBands();
	//second function here:
	function fetchTopBands()
	{
		function displayBand(bandIndex, bandName){
			console.log(bandIndex+ ". " + bandName);
		}

		let bandIndex = 0;
		displayBand(++bandIndex, "Reel Big Fish");
		displayBand(++bandIndex, "Ken Ascorp");
		displayBand(++bandIndex, "NateWantsToBattle");
		displayBand(++bandIndex, "NinjaSexParty");
		displayBand(++bandIndex, "Static-P");

	}


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/



	fetchTopMovies();		
	function fetchTopMovies(){

		function displayMovie(movieIndex, movieName, movieRating){

			console.log(movieIndex+ ". " + movieName);
			console.log("Rotten Tomatoes Rating "+movieRating+"%");

		}

	let movieCount = 0;

	displayMovie(++movieCount,"Sword of the Stranger",91);
	displayMovie(++movieCount,"One Piece Film Red",94);
	displayMovie(++movieCount,"Gintama: The Movie: The Final Chapter: Be Forever Yorozuya", 89);
	displayMovie(++movieCount,"Everything Everywhere All at Once",94);
	displayMovie(++movieCount,"Nobody",84);

	}

	

	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


//console.log(friend1);
//console.log(friend2);