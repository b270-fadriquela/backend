/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

		*/

		let sumOfTwoDigit = addNum(45,2);
		let diffOfTwoDigit = subtractNum(55,25);
		let prodOfTwoDigit = multiplyNum(17,4);
		let ratioOfTwoDigit = divideNum(100,2);

		console.log("The Sumation of Two Digit: " + 45 + " and " + 2 + " is:");
		console.log(sumOfTwoDigit);
		console.log("The Difference of Two Digit: " + 55 + " and " + 25 + " is:");
		console.log(diffOfTwoDigit);
		console.log("The Product of Two Digit: " + 17 + " and " + 4 + " is:");
		console.log(prodOfTwoDigit);
		console.log("The Ratio of Two Digit: " + 100 + " and " + 2 + " is:");
		console.log(ratioOfTwoDigit);

		function addNum(digit1, digit2){
			return digit1 + digit2;

		}

		function subtractNum(digit1, digit2){
			return digit1 - digit2;

		}

		function multiplyNum(digit1, digit2){
			return digit1 * digit2;
		}

		function divideNum(digit1, digit2){
			return digit1 / digit2;
		}

		/*

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	


	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
	*/

		let circleArea = calcAreaOfCircle(15);
		function calcAreaOfCircle(radius){
			const pi = 3.14;
			return pi * (radius**2);
		}

			console.log("The Result of Getting the area of a circle with the radius of " + 15 + " is: ");
			console.log(circleArea);

	/*
	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	
	*/


	//let numbers = [20, 40, 60, 80];
	let averageVar = calcAveofNumbers(20, 40, 60, 80);

	// function calcAveofNumbers(numberArr){

	// 	return (numberArr[0] + numberArr[1] + numberArr[2] + numberArr[3]) / numberArr.length;
	// }

	function calcAveofNumbers(firstNum, secondNum, thirdNum, forthNum){

		return (firstNum +  secondNum+ thirdNum+ forthNum) / 4;
	}

	console.log("The average of 20, 40, 60 and 80:");
	console.log(averageVar);


	/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	let isPassingScore = isStudentPassing(38,50);
	function isStudentPassing(score, total){

		let percentage = ((score / total) * 50) + 50
		let isPass;
		if(percentage > 75)
			isPass = true;
		else 
			isPass = false;

		return isPass;
	}

	console.log(isPassingScore);


	function displayedProduct(num1, num2) {
        console.log("The product of " + num1 +" and " + num2);
        let result = num1 * num2;
        return result;
    }

    let result = displayedProduct(50, 10);
    console.log("[]" + result);