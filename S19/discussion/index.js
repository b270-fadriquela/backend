// console.log("Happy Friday!");

// What are conditional statements?
// Conditional Statements allow us to control the flow of our program. They also allow us to run statement/instructons if a condition is met or run another separate instruction if otherwise. 

// [SECTION] if, else if and else statement


let numA = -1;

if(numA < 0){
	console.log("Hello!");
}
console.log(numA < 0);

// if statement - executes if a specified condition is true.

/*
Syntax :
	if(condition) {
		statement
	}
*/


let city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan!");
}

// else if statement
// Executes a statement if previous conditions are false and if the specified condition is true

let numH = 1;

if(numH < 0) {
	console.log("Hello again!");
} else if (numH > 0) {
	console.log("World");
}


// else statement
if (numA > 0) {
	console.log("Hello");
} else if (numA === 0) {
	console.log("World");
} else {
	console.log("Again");
}

// Will result to an error
// else {
// 	console.log("Hi, B270!");
// }

// if, else if and else statements with functions

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {

		return "Not a typhoon yet."; 

	} else if (windSpeed <= 61) {

		return "Tropical depression detected.";

	} else if (windSpeed >= 62 && windSpeed <= 88) {

		return "Tropical storm detected.";

	} else if (windSpeed >= 89 || windSpeed >=117) {

		return "Severe tropical storm detected.";

	} else {

		return "Typhoon detected.";
	}
}

// Returns the string t the variable message
let message = determineTyphoonIntensity(110);
console.log(message);

if(message == "Severe tropical storm detected.") {
	console.warn(message);
}