

let number = prompt("Enter a Number");
loopActivity(number);


//Divisible by 5
//Divisible 10
//Less than or equal to 50

//store consonants from string
//



function loopActivity(userInput){

	console.log("The Number you provided: " + userInput);

	for (var index = userInput; index >= 0; index--) 
	{

		if(userInput <= 50){
			break;
		}

		if(index % 10 == 0)
		{
			console.log("the number is being skipped ❌");
			continue;
		}
		if(index % 5 == 0){
			console.log(index + "✅");
		}

	}
}


let extremelyLongWord = "supercalifragilisticexpialidocious";
stringCheckActivity(extremelyLongWord);

function CheckVowel(character)
{

	 if(character.toLowerCase() === "a"|| 
	 	character.toLowerCase() === "e"|| 
		character.toLowerCase() === "i"|| 
		character.toLowerCase() === "u"|| 
		character.toLowerCase() === "o" )
	 {

	 		return true;
	 } 
	 else
	 {
	 		return false;
	 }

}

function stringCheckActivity(message){

	console.log(extremelyLongWord);
	var storedConsonants = "";
	for (var index = 0; index < message.length; index++) {

		if(CheckVowel(message[index]))
		{
			continue;
		}
		else
		{
			storedConsonants += message[index];
		}
	}

	console.log(storedConsonants);

}



// ✅ 1. In the S20 folder, create an activity folder and an index.html and index.js file inside of it.
// ✅ 2. Link the index.js file to the index.html file.
// ✅ 3. Create a variable number that will store the value of the number provided by the user via prompt.
// ✅ 4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
// ✅ 5. Create a condition that if the current value is less than or equal to 50, stop the loop.
// ✅ 6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
// ✅ 7. Create another condition that if the current value is divisible by 5, print the number.
// ✅ 8. Create a variable that will contain the string supercalifragilisticexpialidocious.
// ✅ 9. Create another variable that will store the consonants from the string.
// ✅ 10. Create another for loop that will iterate through the individual letters of the string based on its length.
// ✅ 11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
// ✅ 12. Create an else statement that will add the letter to the second variable.
// ✅ 13. Create a git repository named S20.
// ✅ 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// ✅ 15. Add the link in Boodle.