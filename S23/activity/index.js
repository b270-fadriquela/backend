


let trainer = {
    age: 13,
    pokeTeam: [
        new Pokemon("Pikachu", 12, "Lighting"),
        new Pokemon("Charizard", 7, "Fire"),
        new Pokemon("Squirtle", 5, "Water"),
        new Pokemon("Bulbasaur", 10, "Grass"),
    ],
      
    talk: function talk(pokemon, otherKid){
        console.log("Trainer " + otherKid.name + " wants to battle!");
        console.log(pokemon.pkName +"! I choose you!");
    }

}

let rival = {
    name: "Gary",
    age: 14,
   
      
    talk: function talk(pokemon, otherKid){
        console.log("Trainer " + otherKid.name + " wants to battle!");
        console.log(pokemon.pkName +"! I choose you!");
    }

}



console.log(trainer);

console.log("Results of dot Notation:");
trainer.name = "Ash";
console.log(trainer.name);

rival["pokeTeam"] = 
[
    new Pokemon("Squirtle", 24, "Water"),
    new Pokemon("Pidey", 7, "Flying"),
];


trainer.talk(trainer.pokeTeam[0],rival);

//trainer.pokeTeam[0].Tackle(rival.pokeTeam[0]);
//Battle(trainer,rival);





function Pokemon(name, level, type){

    this.pkName = name;
    this.lvl = level;
    this.pkTyping = type.toLowerCase();

    this.atk = level;
    this.health = level * 2;

    this.speed = level * 0.5;

    //return true if pokemon is unable to battle
    this.Tackle = function(target){

        if(target.health <=0)
            //target pokemon is no longer able to battle
            return true;
        else if(this.health <=0){
            //my pokemon can no longer do battle
            return true;
        }
        else{

            console.log(this.pkName + " use Tackle! ");

            console.log(this.pkName + " tackled "  + target.pkName);

            let dmgTaken = this.atk;
            switch(target.pkTyping)
            {
                case "lighting":                     
                    if(this.pkTyping == "ground")
                    {
                        console.log("It's Super Effective");
                        target.health -= dmgTaken * 2;
                    }
                    else
                        target.health -= dmgTaken;
                        console.log(target.pkName + " took "  + dmgTaken + " Dmg");
                    break;
                case "water":

                    if(this.pkTyping == "lighting")
                    {
                        console.log("It's Super Effective");
                        target.health -= dmgTaken * 2;
                    }  else
                        target.health -= dmgTaken;
                        console.log(target.pkName + " took "  + dmgTaken + " Dmg");

                    break;
                    default:
                    {
                        target.health -= dmgTaken;
                        console.log(target.pkName + " took "  + dmgTaken + " Dmg"); 
                    }

            }

            console.log(target.pkName + " hp is now "  + target.health);   

            if(target.health <=0){
                 target.faint();
            }

            return false
        }
    }

      this.faint = function(){

        console.log(this.pkName + " fainted!");
    };




}



function Battle(trainer, rival)
{

    let battleDone = false;
        console.log("===start of battle===");

    while(!battleDone){

        // if(rival.pokeTeam.length > 0 && trainer.pokeTeam.length > 0)
        //     battleDone = false;

        battleDone = trainer.pokeTeam[0].Tackle(rival.pokeTeam[0]);
        rival.pokeTeam[0].Tackle(trainer.pokeTeam[0]);


        console.log("===end of turn===");

    }
       
}