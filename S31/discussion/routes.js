const http = require("http");

const port = 4000;

const server = http.createServer((request,response) => {


    if(request.url == '/greeting')
    {
        response.writeHead(200,{'Content-Type': 'text/plain'});
        
    }
    else if(request.url == '/homepage')
    {
        response.writeHead(200,{'Content-Type': 'text/plain'});
        response.end("This is the homepage");
    }  
    else if(request.url == '/profile')
    {
        response.writeHead(200,{'Content-Type': 'text/plain'});
        response.end("This is the profilepage");
    }
    else
    {  
        response.writeHead(404, {'Content-Type': 'text/plain'});
    }

})

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);