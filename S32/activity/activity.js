let http = require('http');
let port = 4000;

let courses = [
    {
        "courseCode": "math",
    },
]

let server = http.createServer((request, response) => {

    // - If the url is http://localhost:4000/, send a response Welcome to Booking System
    if (request.url == "/" && request.method == "GET"){
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Welcome to Booking System");
    }


    // - If the url is http://localhost:4000/profile, send a response Welcome to your profile!
    if (request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write("Welcome to your profile!");
        response.end();
    }


    // - If the url is http://localhost:4000/courses, send a response Here’s our courses available
    if (request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write("Here’s our courses available");
        response.end();
    }



    // - If the url is http://localhost:4000/addCourse, send a response Add a course to our resources
    if (request.url == "/addCourse" && request.method == "POST"){
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write("Add a course to our resources");
        response.end();
    }

    // - If the url is http://localhost:4000/updateCourse, send a response Update a course to our resources
    if (request.url == "/updateCourse" && request.method == "PUT"){
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write("Update a course to our resources");
        response.end();
    }

    // - If the url is http://localhost:4000/archiveCourses, send a response Archive courses to our resources
    if (request.url == "/archiveCourses" && request.method == "DELETE"){
         
            response.writeHead(200, {'Content-Type': 'application/json'})
            response.write("Archive courses to our resources");
            response.end();
    }


})
server.listen(port);
console.log(`!Server running at localhost:${port}`);
