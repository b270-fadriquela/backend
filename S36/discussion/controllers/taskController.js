// Controllers contain the functions and business logic of our Express application
// All the operations that it can do will be placed in this file
const Task = require("../models/task");

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
}


module.exports.createTask = (requestBody) => {

    let newTask = new Task({

        name: requestBody.name
    })

    return newTask.save().then((task,error) =>{

        if(error){
            console.log(error);
            return false;
        }
        else{

            return task;
        }

    })

}

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask,err) =>{

        if(err){

            console.log(err);
            return false;

        }
        else{
            return removedTask;
        }

    });

}

module.exports.updateTask = (taskId, newContent) => {


    return Task.findById(taskId).then((result,err) =>{

        if(err)
        {
            console.log(err);
            return false;

        }
        else
        {
            result.name = newContent.name;

            return result.save().then((updatedTask,savErr) =>{

                if(savErr){
                    console.log(savErr);
                    return false;
                }
                else{

                    return updatedTask;
                }   

            });
        }
    })
}


///ACTIVITY


// 2. Create a controller function for retrieving a specific task.

module.exports.getTask = (taskId) => {
     return Task.findById(taskId).then((task,err) =>{

        if(err){
            console.log(err);
            return false;
        }
        else{
            return task;

        }


     });
}


// 6. Create a controller function for changing the status of a task to "complete".
module.exports.completeTask = (taskId,newStatus) => {


     return Task.findById(taskId).then((curTask,err) =>{

        if(err)
        {
            console.log(err);
            return false;

        }
        else
        {
            curTask.status = newStatus;

            return curTask.save().then((updatedTask,completeErr) =>{

                if(completeErr){
                    console.log(completeErr);
                    return false;
                }
                else{

                    return curTask;
                }   

            });
        }
    })
}

