const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require("./routes/taskRoutes")

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin123@zuitt.veho36n.mongodb.net/b270-to-do?retryWrites=true&w=majority",
                {
                    useNewUrlParser : true,
                    useUnifiedTopology: true,
                });



app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks", taskRoute);


//mongodb+srv://admin:<password>@zuitt.veho36n.mongodb.net/<collectionName>?retryWrites=true&w=majority


app.listen(port, () => console.log("Now listening"))