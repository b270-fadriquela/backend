// Contains all the endpoints for our application

const express = require("express");

// The "taskController" allows us to use the functions defined in the "taskController.js"
const taskController = require("../controllers/taskController");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// [SECTION] Routes

// Route to get all the tasks
router.get("/", (req, res) => {

    // Invokes the "getAllTasks" function from the "taskController.js" file
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post("/", (req,res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

router.delete("/:id", (req,res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id", (req,res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//ACTIVITY 37

// 1. Create a route for getting a specific task.

router.get("/:id", (req, res) => {

    // Invokes the "getAllTasks" function from the "taskController.js" file
    // 3. Return the result back to the client/Postman.
    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})




// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
// 5. Create a route for changing the status of a task to "complete".
router.post("/:id/:status", (req, res) => {

    // Invokes the "getAllTasks" function from the "taskController.js" file
    // 3. Return the result back to the client/Postman.
    taskController.completeTask(req.params.id,req.params.status).then(resultFromController => res.send(resultFromController));
})


// Exports the router object to be used in the "index.js"
module.exports = router;
