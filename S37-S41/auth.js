const jwt = require("jsonwebtoken");

// User defined string data that will be used to create our JSON web token
// Used is the algorithm for encrypting our data which make difficult to decode the information without the defined secret code
const secret = "CourseBookingAPI";

// [Section] JSON Web Tokens
/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code can decode the encrypted information

- Imagine JWT as a gift wrapping service that secures the gift with a lock
- Only the person who knows the secret code can open the lock
- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
- This ensures that the data is secure from the sender to the receiver
*/

// Token creation
// Analogy: Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {

    // The data will be received from the registration form
    // When the user logs in, a token will be created with the user's information
    // payload
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    // Generate a JWT using the jwt's sign method
    /*
        Syntax:
        jwt.sign(payload, secret, {options/callBackFunctions})
    */
    return jwt.sign(data, secret, {});
}


// Token verification
/*
- Analogy
    Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (req, res, next) => {

    let token = req.headers.authorization;

    if(token !== undefined) {
        console.log(token);

        //Slices the word "Bearer " to get raw token value
         token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return res.send({auth: "failed"});

            } else {

                next();
            }
        })
    } else {
        return res.send({auth:"failed"});
    }
}


// Token decryption
// Analogy: Open the gift and get the content
module.exports.decode = (token) => {

    if(token !== undefined) {

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return null;

            } else {

                return jwt.decode(token, {complete:true}).payload;
            }
        })
    } else {
        return null;
    }
}