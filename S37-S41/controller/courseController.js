const Course = require("../models/Course");
//npm install bcryptnpm
const bcrypt = require("bcrypt");
//npm install jsonwebtoken
const auth = require("../auth.js");


//Activity 39
module.exports.addCourse = (req,res) => {

	const adminData = auth.decode(req.headers.authorization);

	if(!adminData.isAdmin){
		console.log(adminData);
		return res.send(`${adminData.email} is not an admin`);
	}
	else{
		console.log("admin detected");

	    let newCourse = new Course({

	    	name: req.body.name,
	    	description: req.body.description,
	    	price: req.body.price,
	    	slots: req.body.slots

	    });

	      return newCourse.save().then(course => {
	            console.log(course);
	            res.send(true);
	       })
	       .catch(error => {
	            console.log(error)
	            res.send(false);
	        });

	}




}


module.exports.getAllCourses = (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
			return Course.find({}).then(result => res.send(result));
	}
	else{
		return res.send(false);
	}
}

module.exports.getAllActive = (req,res) => {
	return Course.find({isActive:true}).then(result => res.send(result));
}

module.exports.findCourse = (req,res) => {
	return Course.findById(req.params.courseId).then(result => res.send(result));
}

module.exports.updateCourse = (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		let updateCourse = {

			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots

		};

	return Course.findByIdAndUpdate(req.params.courseId, updateCourse)
				 .then(result => res.send(updateCourse));
	}
	else{
		return res.send(false);
	}

}


module.exports.archiveCourse = (req, res) => {


	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		let updateCourse = {
			isActive : false
		};

	return Course.findByIdAndUpdate(req.params.courseId, updateCourse)
				 .then(result => res.send(updateCourse));
	}
	else{
		return res.send(false);
	}

}

