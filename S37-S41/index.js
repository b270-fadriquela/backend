const express = require('express');
const mongoose = require('mongoose');

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

//MongoDB Connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.veho36n.mongodb.net/b270-booking?retryWrites=true&w=majority",
                {
                    useNewUrlParser : true,
                    useUnifiedTopology: true,
                });

//Set notif for db connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We are connected to database"));

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/course", courseRoutes);


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
});

