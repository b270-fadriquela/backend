const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {

        type: String,
        required: [true, "First name is required"]

    },

    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },

    email : {
        type: String,
        required: [true, "email is required"]
    },

    password : {
        type: String,
        required: [true, "password is required"]
    },

    isAdmin : {
        type: Boolean,
        default: false
    },

    mobileNo : {
        type: String,
        required: [true, "mobile number is required"]
    },

    enrollments : [
        {
           courseId: {
                type: String,
                required: [true, "Enrollment CourseId is needed"]
           },
           enrolledOn: {
                type: Date,
                default: new Date()
           },
           status: {
                type: String,
                default: "Enrolled"
           }
        }
    ]


})

module.exports = mongoose.model("Userbase", userSchema);

