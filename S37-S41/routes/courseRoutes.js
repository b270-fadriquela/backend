const express = require("express");
const router = express.Router();
const courseController = require("../controller/courseController");
const authy = require("../auth.js");


router.post("/addCourse",authy.verify,courseController.addCourse);

router.get("/all",authy.verify,courseController.getAllCourses);
router.get("/",courseController.getAllActive);


router.get("/:courseId", courseController.findCourse);
router.post("/:courseId", authy.verify, courseController.updateCourse);

router.patch("/:courseId/archive", authy.verify,courseController.archiveCourse);

module.exports = router;