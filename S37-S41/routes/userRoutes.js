const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");
const authy = require("../auth.js");

//ACTIVITY 38
router.get("/details", (req,res) => authy.verify(req,res,() => userController.getProfile(req,res)));
//router.get("/details", authy.verify,userController.getProfile);

router.post("/checkEmail", (req,res) => {userController.checkEmailExist(req,res)});
router.post("/register",(req,res) => {userController.registerUser(req,res)});
router.post("/login", userController.loginUser);

router.post("/", (req,res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/enroll", authy.verify, userController.enroll);

module.exports = router;